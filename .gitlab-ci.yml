default:
  tags:
    - docker

image: ubuntu:jammy

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DEBEMAIL: code@hxr.io
  DEBFULLNAME: HXR CI

stages:
  - test
  - deploy

lint-job:
  stage: test
  script:
    - apt-get update
    - apt-get install --no-install-recommends --assume-yes shellcheck build-essential
    - pwd
    - make lint
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

release-job:
  stage: deploy
  script:
    - apt-get update
    - apt-get install --no-install-recommends --assume-yes curl ca-certificates
    - date +%Y%m%d%H%M > file.txt
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ./file.txt "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/pass-mount/$CI_COMMIT_TAG/file.txt"'
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Built by automation'
    tag_name: '$CI_COMMIT_TAG'
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+$/

deploy-job:
  stage: deploy
  environment: production
  script:
    - '[[ ! -z "$CI_KEY" ]] || (echo "Warning: \$CI_KEY not set" && exit 1)'
    - apt-get update
    - apt-get install --no-install-recommends --assume-yes gnupg git devscripts build-essential debhelper dput
    - cat $CI_KEY | gpg --batch --import
    - echo $CI_PASS | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s $(mktemp)
    - git config --global user.email "code@hxr.io"
    - git config --global user.name "HXR CI"
    - git fetch origin main
    - git merge -srecursive -Xours --allow-unrelated-histories origin/main
    - ls -la
    - PKG_VERSION="$(cat ./VERSION)-$(date +%Y%m%d%H%M)-ci.${CI_COMMIT_SHORT_SHA}"
    - dch --distribution jammy --newversion $PKG_VERSION automated CI
    - debuild --no-tgz-check -i -S
    - dput ppa:hxr-io/aesthetic-testing ../pass-mount_${PKG_VERSION}_source.changes
  rules:
    - if: $CI_COMMIT_BRANCH == "debian/latest"

debuild-job:
  stage: deploy
  environment: production
  script:
    - '[[ ! -z "$CI_KEY" ]] || (echo "Warning: \$CI_KEY not set" && exit 1)'
    - apt-get update
    - apt-get install --no-install-recommends --assume-yes gnupg git devscripts build-essential debhelper dput libdistro-info-perl
    - cat $CI_KEY | gpg --batch --import
    - echo $CI_PASS | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s $(mktemp)
    - git config --global user.email "code@hxr.io"
    - git config --global user.name "HXR CI"
    - "PKG_VERSION=$(basename $CI_COMMIT_TAG | sed 's/^v//')"
    - git fetch origin v$PKG_VERSION
    - git merge --no-commit -srecursive -Xours --allow-unrelated-histories v$PKG_VERSION
    - dch --distribution jammy --newversion $PKG_VERSION automated CI
    - debuild --no-tgz-check -i -S
    - dput ppa:hxr-io/aesthetic ../pass-mount_${PKG_VERSION}_source.changes
  rules:
    - if: $CI_COMMIT_TAG =~ /^ubuntu\//
